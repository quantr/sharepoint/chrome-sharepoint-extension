document.getElementById("en").onclick = function (element) {
  // var lang = chrome.i18n.getUILanguage();
  chrome.i18n.setUTLanguahe('en-US');
};

document.getElementById("hk").onclick = function (element) {
  // var lang = chrome.i18n.getUILanguage();
  chrome.i18n.setUTLanguahe('zh-HK');
};

document.getElementById("siteContent").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/viewlsts.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("workbench").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/workbench.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("sitepages").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/sitepages";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("peopleAndGroup").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/people.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("siteCollectionAdmins").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/mngsiteadmin.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("advancedPermissions").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/user.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("masterPages").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_Layouts/ChangeSiteMasterPage.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("titleDesc").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/prjsetng.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("navigation").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/AreaNavigationSettings.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("pageLayout").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_Layouts/AreaTemplateSettings.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("welcomePage").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_Layouts/AreaWelcomePage.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("treeView").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/navoptions.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("topNavBar").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/topnav.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("siteTheme").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/themeweb.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("resetToSiteDefinition").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/reghost.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("searchableColumns").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_Layouts/NoCrawlSettings.aspx";
    chrome.tabs.create({ url: url });
  });
};

document.getElementById("siteContentTypes").onclick = function (element) {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    var url = getSiteUrl(tabs[0].url) + "/_layouts/mngctype.aspx";
    chrome.tabs.create({ url: url });
  });
};

function getSiteUrl(url) {
  url = url.toLowerCase();
  if (url.indexOf('/forms') >= 0) {
    url = url.substring(0, url.indexOf('/forms'));
    return url.replace(/\/forms.*/g, "");
  } else if (url.indexOf('/lists') >= 0) {
    url = url.substring(0, url.indexOf('/lists'));
    return url.replace(/\/lists.*/g, "");
  } else if (url.indexOf('/_layouts') >= 0) {
    url = url.substring(0, url.indexOf('/_layouts'));
    return url.replace(/\/_layouts.*/g, "");
  } else if (url.indexOf('/sitepages') >= 0) {
    url = url.substring(0, url.indexOf('/sitepages'));
    return url.replace(/\/sitepages.*/g, "");
  } else {
    return url;
  }
}

resize = function () {
  document.body.style.width = "250px";
  // document.body.style.height = '1200px';
};

onload = function () {
  setTimeout(resize, 100);
};
